

function single(size, ctx) {



  // Calculate center of image (just by dividing the image size by 2)

  const origo = Math.round(size / 2);

  // Set the size of the big circle the smaller circles should follow
  const bigSize = size / 3; // This is the size (radius in pixels) of the "invisible" big circle which curve the smaller circles follows

  const circleReductionFactor = 0.06; // Each circle is 6 % smaller than the previous one
  const startFactor = 0.189; // The factor size of the start circle. Could have been any number giving a sane result together with the reduction percentage

  const completeLoop = 2 * Math.PI;

  let angleFactor = 0; // The current starting angle factor


  // Start drawing
  drawCircles(startFactor);


  // Function for creating circles in a leg following the path of what we call "the big circle"
  // Note the connection between the angle within the "big circle" to the size of the small circles.
  // We move along the arc of the big circle using the same value as we use for the circle radius
  function drawCircles(currentSizeFactor, end = false) {

    /*     if (!end && currentSizeFactor < 0.01) {
          console.debug('Too sml inc. Manual adjustment')
          currentSizeFactor = 0.01
        } */

    // Push curve ahead equal the factor of this circle
    angleFactor += currentSizeFactor;
    // Now curve is at center of the current small circle

    // Calculate the circle center coordinates along the big circle curve 
    const x = origo + bigSize * Math.cos(angleFactor);
    const y = origo + bigSize * Math.sin(angleFactor);
    const radius = currentSizeFactor * bigSize;

    /// Then draw the circle
    drawCirc(x, y, radius, end ? 'red' : undefined);

    // Push the angle ahead equal the size factor of the current circle,
    angleFactor += currentSizeFactor;
    // Now the current angle point at the end of the current circle (which is also the starting point for the next circle)
    //console.debug('Current size factor', currentSizeFactor);
    //console.debug('Current angleFactor', angleFactor, end)

    if (end) {
      // We're finished!
      return;
    }

    // Calculate the next circle size
    let nextSizeFactor = currentSizeFactor - currentSizeFactor * circleReductionFactor;
    if (nextSizeFactor < 0.0001) {
      nextSizeFactor = 0.0001;
    }

    // Check if adding a new circle will surpass the starting circle after a complete loop
    if (angleFactor + nextSizeFactor * 2 >= completeLoop) {
      // If we do another loop we will pass the start circle
      // We need to finish with adding the difference between the current point and the tangent of the start circle
      return drawCircles((completeLoop - angleFactor) / 2, true)
    }

    // if (nextSizeFactor < 0.0001) throw Error('Less then 0.1 inc')

    // Start over with next circle
    // Reduce the size of the next circle, but do not go below 0.01
    return drawCircles(nextSizeFactor);


  }


  // Function to draw a circle on the canvas
  function drawCirc(x, y, r, color = getRandomColor()) {
    ctx.beginPath();
    ctx.fillStyle = color;
    ctx.arc(x, y, r, 0, 2 * Math.PI);
    ctx.fill(); // You may here use ctx.stroke insted, in order to draw no-filled circles
  }

  function getRandomColor() {
    const letters = '0123456789ABCDEF';
    let color = '#';
    for (var i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
  }

}

