
function getFactors(type) {
  // We have 6 "spokes" or "legs" of smaller circles going from the center and tangents the next leg.
  // The circle in the center of the leg, connecting the outer leg and the inner leg, we call the "king" circle for easy referance.
  // In this example all 6 legs are equal, but this does not need to be the case.
  // The arc of each leg follows a bigger "invisible circle" we call the "big circle".
  // Each number below in the "factors" represent the size of the small visible circles.

  // We have here chosen to mainly have 6 circles on each side of the king circle, but this does not need to be the case

  // The small circles are laid out on 2/3 of the complete curve of the large invisible circle. 
  // A full circle round corresponds to 2PI in radians, so 2/3 rounds equals 4/3*Pi or 1,3333Pi. 
  // But in order for us to more easily set circle sizes with more intuitive numbers, we use a constant in our logic that multiply our factors with PI/15
  // This means the 2/3 of the big circle corresponds to 20 of our factors (4/3 = 20/15)

  // We divide each leg in an "inner" and an "outer" part. Separated by the king.

  // The total length of the inner leg is the radius of the center, all diameters of the circles in the inner leg, and the radius of the king
  // The total length of the outer leg is the radius of the king, all diameters of the circles in the outer leg, and the radius of the king in the next leg in the figure
  // In this excample all legs are equal, so we can use the same radius for both kings, but in another implementation I have made logic with different sized kings.

  // The total sum of all circles from the center of the figure to the center of the king (along the inner leg), therefore needs to be 10
  // The total sum of all the circles from the king's center to the center of the king in the next leg, also needs to be 10. 



  const legLength = 10;
  const legRadii = legLength / 2;

  const myFactors = selectFactorsBasedOnType();
  console.info('Using my factors', myFactors);

  checkFactors(myFactors);

  const factors = convertToRadian(myFactors);

  console.debug('Factors converted are', factors);

  return factors;




  function selectFactorsBasedOnType() {
    switch (type) {
      case 'normal': return getBase();
      case 'crazy': return getAlt1();
      case 'random': return getRandom();
      case 'small': return getMany();

    }
  }







  function getBase() {
    // Below, we can see that the sum of the "inner" and "outer" array are both 4. The numbers are the radius of each circle. The diameters will in total be 8. 
    // Adding the radius of the center and the radius of the king, gives us 8 + 1 + 1 = 10 for the inner leg. 
    // For the outer leg we add the radius of this king and the redius of the next king, also reaching 20.

    // Note: You can set these numbers to whatever you want, as long as each leg has the total circle cizes (or length) of 10 from center to king and from king to king
    // Note that the "king" or "connection circle" needs to be in the middle (at 10 calculated from the center) in order for the previous leg to tangent this leg.

    return {
      center: 1.0, // The center circle
      inner: [0.4, 0.5, 0.6, 0.7, 0.85, 0.95],//From center and outwards. The sum here is 4.
      king: 1.0, // The "connection circle" in the middle of each "leg"
      outer: [0.95, 0.85, 0.7, 0.6, 0.5, 0.4], // From the "connection circle" towards the next "connection circle". The sum here is also 4
    }
  }


  function getAlt1() {
    // The "outer" leg does not need to be equal to the inner leg. And each leg may be different from the others. 
    // Just keep in mind to keep the king in the center.

    // Below is a not so symetric figure to illustrate the logic.

    return {
      center: 0.6,
      inner: [0.3, 0.8, 0.3, 0.7, 0.4, 1.3], // The sum of the radii here is 3,8. Thus, the total length of all the circles (their diameters) are 7.6 And adding the center gives ut 8.2. The king circle therefore needs to be 1.8 in order to reach 10
      king: 1.8,
      outer: [0.4, 0.7, 0.45, 0.45, 0.4, 0.8] // Since the king circle of both the current and the next leg are 1.8, we have already used 3.6 of the distance. The rest of the circles in the outer leg needst to be 6.4 (3.6 + 6.4 = 10). Their radii needs to sum up to 3.2. Which this array does.
    }
  }

  function getRandom() {
    // We here generate random sizes within a set interval


    const center = random(0, 2);
    const king = random(0, 2);
    const inner = createRandomLeg(center + king);
    const outer = createRandomLeg(king * 2);

    return {
      center: center,
      inner: inner,
      king: king,
      outer: outer
    }

    function createRandomLeg(dif) {
      const arr = [];

      do {
        arr.push(random(0.1, 1.3));
      } while (arr.reduce((v, i) => v + i, 0) * 2 + dif < legLength);

      arr.pop();
      arr.push((legLength - dif) / 2 - arr.reduce((v, i) => v + i, 0));

      return arr;
    }
  }

  // This excamples illustrates a different number of circles
  function getMany() {

    const arr1 = [];
    const arr2 = [];
    let i = 0.01;
    const inc = 0.008;
    let sum;
    do {
      arr1.push(i);
      i += inc;
      sum = arr1.reduce((v, i) => v + i, 0);
    } while (sum + i < 5)
    const center = 0;
    const king = legLength - (sum * 2)

    do {
      arr2.push(i);
      i -= inc;
      if (i <= 0) throw Error('I less than 0');
      sum = arr2.reduce((v, i) => v + i, 0);
    } while (sum + i + king < legRadii)

    arr2.push(legRadii - sum - king);

    return {
      center: center,
      inner: arr1,
      king: king,
      outer: arr2,
    };
  }
}


function randomInt(max) {
  return Math.floor(Math.random() * max)
}

function random(min, max) {
  return min + Math.random() * (max - min);
}

function convertToRadian(factors) {
  const k = Math.PI / 15;

  // We multiply the factor number with k.
  // Multiplying with k is just for convenience to let us use factor numbers which are simpler to work with. 

  return {
    center: factors.center * k,
    leg: [...factors.inner.map((v) => v * k), factors.king * k, ...factors.outer.map((v) => v * k)]
  }
}

function checkFactors(factors) {
  // Since the array contains the radius of the circles, we need to multiply them with 2 to get the complete size/length of the circle
  const sumInner = factors.inner.reduce((v, i) => v + i, 0) * 2 + factors.king + factors.center;
  const sumOuter = factors.outer.reduce((v, i) => v + i, 0) * 2 + factors.king * 2;
  // When we create random factors we subtract the current value from 10 to find the last value in the leg array. 
  // Logically this should always lead to a sum of 10: x + (10-x) = 10
  // Due to javascripts logic adding the difference between 10 and a floating number to the same floating number may lead to results like 9.999999999 instead of 10 
  // thus we need roundt the values before the test.
  if (round(sumInner) !== 10 || round(sumOuter) !== 10) {
    console.error('Inner sum: ', sumInner, 'Outer sum', sumOuter);
    throw Error('Invalid factors');
  }
}

function round(num) {
  return Math.round(num * 100000) / 100000
}