(() => {
  // Get input elements
  const sizeInput = document.querySelector('.sizeInput');
  const typeSelect = document.querySelector('.typeSelect');
  const redrawButton = document.querySelector('.redrawButton');

  // Get canvas and create a 2d context
  const canvas = document.getElementsByTagName('canvas')[0];
  const ctx = canvas.getContext('2d');

  sizeInput.addEventListener('change', (event) => run(event.target.value, typeSelect.value));
  typeSelect.addEventListener('change', (event) => run(sizeInput.value, event.target.value));
  redrawButton.addEventListener('click', () => run(sizeInput.value, typeSelect.value))

  // Trigger initial run
  run(sizeInput.value, typeSelect.value);

  function run(size, type) {

    console.clear();

    //ctx.clearRect(0, 0, canvas.width, canvas.height);
    canvas.height = canvas.width = size;

    // Eneble the redraw button if type is random or single
    if (type == 'random' || type == 'single') {
      redrawButton.removeAttribute('hidden');
    } else {
      redrawButton.setAttribute('hidden', true);
    }

    // We use a different simplified logic for the single type
    if (type == 'single') {
      return single(size, ctx);
    }

    // For other types, get factors and run the core function
    const factors = getFactors(type);

    return core(size, factors, ctx);

  }

})()



