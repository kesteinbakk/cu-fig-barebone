
function core(size, factors, ctx) {


  // Calculate pi divided by 3. Since we are going to create 6 legs within a circle, and a compelte circle is 2PI, each leg is 2PI/6=PI/3 radians apart.
  const PId3 = Math.PI / 3;

  // Calculate sizes for the figure. We could have used ctx.canvas.clientWidth instead of passing in the size, but passing the size seemed more intuitive for readers

  const bigSize = size / 5; // This is the size of the "invisible" big circle which curve the smaller circles follows

  // Calculate center of image (just by deviding the image size by 2)
  const origo = Math.round(size / 2);

  // Some variables to hold information while we create the figure
  let angle = 0; // The current angle
  let leg = 1; // The number of the current leg

  const stopAfterLeg = 6; // We can either keep track of the leg number or stop when the angle have completed a full circle (2*PI)

  // Draw center circle
  const centerSize = bigSize * factors.center;
  drawCirc(origo, origo, centerSize);

  // Start drawing legs
  createLeg(angle, leg);

  function createLeg(angle, leg) {
    //console.log('Angle', angle, 'Leg:', leg);

    const legX = origo + (bigSize * Math.cos(angle));
    const legY = origo + (bigSize * Math.sin(angle));

    const legAngle = angle - Math.PI;
    // PI inverts leg center angle compared to the starting angle

    // We then move the angle the length of the center circle which we which we have already drawn.
    const startAngle = legAngle + factors.center;

    // Draw circles in this leg
    createCircles(legX, legY, startAngle);

    // Check if we are finished
    if (++leg > stopAfterLeg) {
      return
    }

    // Move to next leg
    angle += PId3;
    return createLeg(angle, leg)
  }


  // Function for creating circles in a leg following the path of what we call "the big circle"
  function createCircles(legCenterX, legCenterY, legAngle, legFactors = [...factors.leg]) {

    // Get the factor for current circle
    const factor = legFactors.shift();

    // Check if factor array was empty - if so we're finished with this leg
    if (!factor) {
      return;
    }

    // Now starts the interesting part. Keep up... 
    // Note the connection between the angle within the "big circle" to the size of the small circles.
    // We move along the arc of the big circle and at the same time we know the radius of the small circles

    // Calculate the size of the small circle
    const radius = factor * bigSize;

    // Increase the angle of the big circle equal to the factor of the current small circle
    legAngle += factor;
    // Now curve is at center of the current small circle

    // Calculate coordinates along the big circle curve at this angle 
    const x = legCenterX + bigSize * Math.cos(legAngle);
    const y = legCenterY + bigSize * Math.sin(legAngle);

    /// Then draw the circle
    drawCirc(x, y, radius);


    // Push the angle further ahead equal the radius of the current small circle,
    legAngle += factor;
    // Now curve is at end of the circle

    // Start over with next circle
    return createCircles(legCenterX, legCenterY, legAngle, legFactors);

  }


  // Function to draw a circle on the canvas
  function drawCirc(x, y, r) {
    ctx.beginPath();
    ctx.fillStyle = "red";
    ctx.arc(x, y, r, 0, 2 * Math.PI);
    ctx.fill(); // You may here use ctx.stroke insted, in order to draw no-filled circles
  }

};